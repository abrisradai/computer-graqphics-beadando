#version 330 core

// VBO-b�l �rkez� v�ltoz�k
in vec3 vs_in_pos;
in vec3 vs_in_norm;
in vec2 vs_in_tex;

// a pipeline-ban tov�bb adand� �rt�kek
out vec3 vs_out_pos;
out vec3 vs_out_norm;
out vec2 vs_out_tex;
//out float d; 

// shader k�ls� param�terei
uniform mat4 MVP;
uniform mat4 world;
uniform mat4 worldIT;

uniform sampler2D texImage;

vec3 GetSpherePosition(float u, float v) {
	u *= 2 * 3.1415f;
	v *= 3.1415f;
	float cu = cos(u), su = sin(u), cv = cos(v), sv = sin(v);
	float R = 1;
	return vec3(R*cu*sv, R*cv,R*su*sv );
}

vec3 GetNormal(float u, float v) {

	vec3 du = GetSpherePosition(u + 0.01, v) - GetSpherePosition(u - 0.01, v);
	vec3 dv = GetSpherePosition(u, v + 0.01) - GetSpherePosition(u, v - 0.01);

	//return normalize(cross(du, dv));

	u *= float(2 * 3.1415);
    v *= float(3.1415);
    float cu = cos(u), su = sin(u), cv = cos(v), sv = sin(v);
    return vec3(cu*sv, cv, su*sv);

}

void main()
{
	vec3 texture_Color= texture(texImage, vs_in_tex).rgb;
	float d = 0.1*length(texture_Color);
	gl_Position = MVP * vec4((1 + d) *  vs_in_pos, 1 );
	
	vs_out_pos = (world * vec4((1 + d) * vs_in_pos, 1)).xyz;
	vs_out_norm = (worldIT * vec4(GetNormal(vs_in_norm.x, vs_in_norm.y), 0)).xyz;
	vs_out_tex = vs_in_tex;
}