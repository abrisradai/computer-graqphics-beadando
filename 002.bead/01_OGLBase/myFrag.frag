#version 330 core

// pipeline-b�l bej�v� per-fragment attrib�tumok
in vec3 vs_out_pos;
in vec3 vs_out_norm;
in vec2 vs_out_tex;
//in float d; 

out vec4 fs_out_col;

// ir�ny f�nyforr�s: f�ny ir�nya
uniform vec3 light_pos = vec3(0,10,5);

// f�nytulajdons�gok: ambiens, diff�z, ...
uniform vec3 La = vec3(1, 1, 1);
uniform vec3 Ld = vec3(0.8, 0.8, 0.8);

uniform sampler2D texImage;
uniform sampler2D texLawn;
uniform sampler2D texMud;
uniform sampler2D texCliff;

// a sz�g sz�m�t�shoz kell �rt�kek
float pi = 3.1415;
float tau;
float alpha;

void main()
{
	// az egyik f�lteken hozz� kell adni pi/2 - t (ahol fi > pi / 2 && fi < 3 * pi / 2) pi / 2<tau<3 *pi /2
	// ^itt ki kell vonni pi-t => ki kell vonni pi / 2 -t
	// a m�sikon ki kell vonni -pi / 2 < tau < pi / 2
	// aza mind a 2 oldalon ki kell szedni pi / 2 -t

	vec3 texture_Color= texture2D(texImage, vs_out_tex).rgb;
	float d = length(texture_Color);

	vec3 Ka = vec3(0,0,clamp(1.0f - d, 0.0f, 1.0f));
	vec3 ambient = Ka * La;

	vec3 normal = normalize(vs_out_norm);
	vec3 to_light = -normalize(vs_out_pos-light_pos);
	
	float cosa = clamp(dot(normal, to_light), 0, 1);

	vec3 diffuse = cosa*Ld;

	tau = acos(vs_out_pos.y / (1 + d)) - pi / 2; // [-90,90] = acos[ 0, pi] - pi / 2
	alpha = abs(sin(tau));

	if (tau <= pi / 2 && tau >= 0){
		fs_out_col = vec4(ambient + diffuse, 1) * 
		( (1 - alpha) * texture(texMud, vs_out_tex) + alpha * texture(texLawn, vs_out_tex));
	} else {
		fs_out_col = vec4(ambient + diffuse, 1) * 
		( (1 - alpha) * texture(texMud, vs_out_tex) + alpha * texture(texCliff, vs_out_tex));
	}

	
	//fs_out_col = vec4(ambient + diffuse, 1) * texture(texImage, vs_out_tex);
}