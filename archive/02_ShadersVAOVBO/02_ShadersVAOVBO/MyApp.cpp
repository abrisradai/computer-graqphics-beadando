#include "MyApp.h"
#include "GLUtils.hpp"

#include <math.h>
#include <vector>

CMyApp::CMyApp(void)
{
	m_vaoID = 0;
	m_vboID = 0;
	m_programID = 0;
}


CMyApp::~CMyApp(void)
{
}

bool CMyApp::Init()
{
	// t�rl�si sz�n legyen k�kes
	glClearColor(0.125f, 0.25f, 0.5f, 1.0f);

	glEnable(GL_CULL_FACE); // kapcsoljuk be a hatrafele nezo lapok eldobasat
	glEnable(GL_DEPTH_TEST); // m�lys�gi teszt bekapcsol�sa (takar�s)
	glDepthFunc(GL_ALWAYS); // mindig �rja fel�l a z buffert - nem felt�tlen a legjobb megold�s,
							// az eleg�ns megold�s, ha a z �rt�keket v�ltoztatjuk 

	//
	// geometria letrehozasa
	//

	std::vector<Vertex> vertices;
	vertices.push_back({ glm::vec3(-1, -1, 0), glm::vec3(1, 0, 0) });
	vertices.push_back({ glm::vec3(1, -1, 0), glm::vec3(0, 1, 0) });
	vertices.push_back({ glm::vec3(-1,  1, 0), glm::vec3(0, 0, 1) });
	vertices.push_back({ glm::vec3(1,  1, 0), glm::vec3(1, 1, 1) });

	// szem rajzol�sa
	vertices.push_back({ glm::vec3(-0.3, 0.3, 0), glm::vec3(1, 1, 1) });
	for (int i = 0; i < 10; ++i) {
		vertices.push_back({ glm::vec3(cos(2 * M_PI / 9 * i) * 0.2 - 0.3, sin(2 * M_PI / 9 * i) * 0.2 + 0.3, 0), glm::vec3(1,1,1) });
	}

	vertices.push_back({ glm::vec3(0.3, 0.3, 0), glm::vec3(1, 1, 1) });
	for (int i = 0; i < 10; ++i) {
		vertices.push_back({ glm::vec3(cos(2 * M_PI / 9 * i) * 0.2 + 0.3, sin(2 * M_PI / 9 * i) * 0.2 + 0.3, 0), glm::vec3(1,1,1) });
	}
	// szemek v�ge

	// cs�pokrajzol�sa
	vertices.push_back({ glm::vec3(-0.5, 0, 0), glm::vec3(1, 0, 0) });
	vertices.push_back({ glm::vec3(-0.5, -0.5, 0), glm::vec3(1, 0, 0) });
	vertices.push_back({ glm::vec3(-0.25, 0, 0), glm::vec3(1, 0, 0) });

	vertices.push_back({ glm::vec3(-0.5 + 0.25, 0, 0), glm::vec3(1, 0, 0) });
	vertices.push_back({ glm::vec3(-0.5 + 0.25, -0.5, 0), glm::vec3(1, 0, 0) });
	vertices.push_back({ glm::vec3(-0.25 + 0.25, 0, 0), glm::vec3(1, 0, 0) });

	vertices.push_back({ glm::vec3(-0.5 + 0.5, 0, 0), glm::vec3(1, 0, 0) });
	vertices.push_back({ glm::vec3(-0.5 + 0.5, -0.5, 0), glm::vec3(1, 0, 0) });
	vertices.push_back({ glm::vec3(-0.25 + 0.5, 0, 0), glm::vec3(1, 0, 0) });

	vertices.push_back({ glm::vec3(-0.5 + 0.75, 0, 0), glm::vec3(1, 0, 0) });
	vertices.push_back({ glm::vec3(-0.5 + 0.75, -0.5, 0), glm::vec3(1, 0, 0) });
	vertices.push_back({ glm::vec3(-0.25 + 0.75, 0, 0), glm::vec3(1, 0, 0) });


	// 1 db VAO foglalasa
	glGenVertexArrays(1, &m_vaoID);
	// a frissen gener�lt VAO beallitasa akt�vnak
	glBindVertexArray(m_vaoID);
	
	// hozzunk l�tre egy �j VBO er�forr�s nevet
	glGenBuffers(1, &m_vboID); 
	glBindBuffer(GL_ARRAY_BUFFER, m_vboID); // tegy�k "akt�vv�" a l�trehozott VBO-t
	// t�lts�k fel adatokkal az akt�v VBO-t
	glBufferData( GL_ARRAY_BUFFER,	// az akt�v VBO-ba t�lts�nk adatokat
				  vertices.size() * sizeof(Vertex),		// ennyi b�jt nagys�gban
				  &vertices[0],	// err�l a rendszermem�riabeli c�mr�l olvasva
				  GL_STATIC_DRAW);	// �gy, hogy a VBO-nkba nem tervez�nk ezut�n �rni �s minden kirajzol�skor felhasnz�ljuk a benne l�v� adatokat
	

	// VAO-ban jegyezz�k fel, hogy a VBO-ban az els� 3 float sizeof(Vertex)-enk�nt lesz az els� attrib�tum (poz�ci�)
	glEnableVertexAttribArray(0); // ez lesz majd a poz�ci�
	glVertexAttribPointer(
		(GLuint)0,				// a VB-ben tal�lhat� adatok k�z�l a 0. "index�" attrib�tumait �ll�tjuk be
		3,				// komponens szam
		GL_FLOAT,		// adatok tipusa
		GL_FALSE,		// normalizalt legyen-e
		sizeof(Vertex),	// stride (0=egymas utan)
		0				// a 0. index� attrib�tum hol kezd�dik a sizeof(Vertex)-nyi ter�leten bel�l
	); 

	// a m�sodik attrib�tumhoz pedig a VBO-ban sizeof(Vertex) ugr�s ut�n sizeof(glm::vec3)-nyit menve �jabb 3 float adatot tal�lunk (sz�n)
	glEnableVertexAttribArray(1); // ez lesz majd a sz�n
	glVertexAttribPointer(
		(GLuint)1,
		3, 
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(void*)(sizeof(glm::vec3)) );

	glBindVertexArray(0); // felt�lt�tt�k a VAO-t, kapcsoljuk le
	glBindBuffer(GL_ARRAY_BUFFER, 0); // felt�lt�tt�k a VBO-t is, ezt is vegy�k le

	//
	// shaderek bet�lt�se
	//
	GLuint vs_ID = loadShader(GL_VERTEX_SHADER,		"myVert.vert");
	GLuint fs_ID = loadShader(GL_FRAGMENT_SHADER,	"myFrag.frag");
	//GLuint mvs_ID = loadShader(GL_VERTEX_SHADER,		"sh.vert");
	//GLuint mfs_ID = loadShader(GL_FRAGMENT_SHADER,		"sh.frag");
	// a shadereket t�rol� program l�trehoz�sa
	m_programID = glCreateProgram();

	// adjuk hozz� a programhoz a shadereket
	glAttachShader(m_programID, vs_ID);
	glAttachShader(m_programID, fs_ID);

	// attributomok osszerendelese a VAO es shader kozt
	glBindAttribLocation( m_programID, 0, "vs_in_pos");
	glBindAttribLocation( m_programID, 1, "vs_in_col");

	// illessz�k �ssze a shadereket (kimen�-bemen� v�ltoz�k �sszerendel�se stb.)
	glLinkProgram(m_programID);

	// linkeles ellenorzese
	GLint infoLogLength = 0, result = 0;

	glGetProgramiv(m_programID, GL_LINK_STATUS, &result);
	glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (GL_FALSE == result )
	{
		char* error = new char[infoLogLength];
		glGetProgramInfoLog(m_programID, infoLogLength, NULL, error);
		std::cerr << "[app.Init()] Probl�ma a shader l�trehoz�sakor: " << error << std::endl;
		delete[] error;
	}

	

	offset[0] = 0;
	offset[1] = 0;

	// mar nincs ezekre szukseg
	glDeleteShader( vs_ID );
	glDeleteShader( fs_ID );

	return true;
}

void CMyApp::Clean()
{
	glDeleteBuffers(1, &m_vboID);
	glDeleteVertexArrays(1, &m_vaoID);

	glDeleteProgram( m_programID );
}

void CMyApp::Update()
{
}


void CMyApp::Render()
{
	// t�r�lj�k a frampuffert (GL_COLOR_BUFFER_BIT) �s a m�lys�gi Z puffert (GL_DEPTH_BUFFER_BIT)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// shader bekapcsolasa
	glUseProgram( m_programID );

	GLuint locT = glGetUniformLocation(m_programID, "t");
	glUniform1f(locT, SDL_GetTicks() / 1000.0);

	glUniform2f(glGetUniformLocation(m_programID, "offset"), offset[0], offset[1]);

	// kapcsoljuk be a VAO-t (a VBO j�n vele egy�tt)
	glBindVertexArray(m_vaoID);

	// kirajzol�s
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glDrawArrays(GL_TRIANGLE_FAN, 4, 11);
	glDrawArrays(GL_TRIANGLE_FAN, 15, 11);
	glDrawArrays(GL_TRIANGLES, 26, 12);
	// VAO kikapcsolasa
	glBindVertexArray(0);

	// shader kikapcsolasa
	glUseProgram( 0 );
}

void CMyApp::KeyboardDown(SDL_KeyboardEvent& key)
{
	switch (key.keysym.sym)
	{
	case SDLK_DOWN:
		offset[1] -= 0.01;
		break;
	case SDLK_UP:
		offset[1] += 0.01;
		break;
	case SDLK_LEFT:
		offset[0] -= 0.01;
		break;
	case SDLK_RIGHT:
		offset[0] += 0.01;
		break;
	default:
		break;

	}
}

void CMyApp::KeyboardUp(SDL_KeyboardEvent& key)
{
}

void CMyApp::MouseMove(SDL_MouseMotionEvent& mouse)
{

}

void CMyApp::MouseDown(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseUp(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseWheel(SDL_MouseWheelEvent& wheel)
{
}

// a k�t param�terbe az �j ablakm�ret sz�less�ge (_w) �s magass�ga (_h) tal�lhat�
void CMyApp::Resize(int _w, int _h)
{
	glViewport(0, 0, _w, _h );
}