#version 130

in vec3 vs_out_col;
in vec3 vs_out_pos;

out vec4 fs_out_col;

uniform float t;
uniform vec2 offset;

vec2 mul(vec2 u, vec2 v){
	return vec2(u.x*v.x - u.y*v.y, u.x*v.y + u.y*v.x);
}

void main()
{
	//fs_out_col = vec4(vs_out_col, 1);

	//h�tt�r sz�n
	/* * /
	if(sqrt(pow(vs_out_pos.x, 2) + pow(vs_out_pos.y, 2)) < sin(2*t*3.1415)*0.5 + 0.5){
		discard; //eldobja a fragmentet
	}
	/ * */

	//MandelBrot Halmaz
	vec2 z = vs_out_pos.xy/t+offset;
	vec2 c = z;
	for(int i = 0; i < 30; ++i){
		z = mul(z,z) + c;
	}

	if(length(z) < 1){
		fs_out_col = vec4(vs_out_col, 1);
	} else {
		discard;
	}
}

// 1. feladat: rajzoljuk ki feh�rrel a t�glalapot!

// 2. feladat: uniform v�ltoz�k - az alkalmaz�s �ll�tsa be, hogy milyen sz�nnel t�lts�nk ki!

// 3. feladat: rajzoljuk ki az orig� k�z�ppont�, 1 sugar� k�rt! Mit kell tenni, ha nem a
//    k�rlapot, hanem csak a k�rvonalat akarjuk? Eml.: discard eldobja a fragmentet

// 4. feladat: komplex sz�mok....
