#version 130

// bemeneti v�ltoz� - most a vertex shader-b�l (vagyis ottani out)
in vec4 vs_out_col;
in vec2 vs_out_pos;

// kimeneti v�ltoz� - a fragment sz�ne
out vec4 fs_out_col;

vec2 sq(vec2 c)
{
	return vec2(c.x*c.x-c.y*c.y,2*c.x*c.y);
}

void main()
{
	//				  R, G, B, A
	fs_out_col = vs_out_col;

	//				  R, G, B, A
	fs_out_col = vec4(1, 1, 1, 1);
	bool ff = false;
	float R = 0.5;
	float eps = 0.07;

	if(length(vs_out_pos) <= R && length(vs_out_pos) >= R - eps){
	fs_out_col = vec4(1, 0, 0, 1);
	}
	else{
	fs_out_col = vec4(1, 1, 1, 1);
	}
	/*
	bool ff = false;

	ff = abs(length(vs_out_pos) - R) < eps
	fs_out_col = ff ? vec4(1, 0, 0, 1) : vec4(1, 1, 1, 1);
	*/
	// MandelBrot Halmaz
	vec2 c = 2*vs_out_pos;
	vec2 xn = c;
	for(int i = 0; i < 100; ++ i){
		xn = sq(xn) + c;
	}
	ff = length(xn) < 2 ;
	fs_out_col = ff ? vec4(1, 0, 0, 1) : vec4(1, 1, 1, 1);
}