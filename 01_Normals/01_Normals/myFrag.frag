#version 140

// pipeline-ból bejövõ per-fragment attribútumok
in vec3 vs_out_pos;
in vec3 vs_out_norm;
in vec2 vs_out_tex;

// kimenõ érték - a fragment színe
out vec4 fs_out_col;

// irány fényforrás: fény iránya
uniform vec3 light_dir = vec3(-1,-1,-1);
uniform vec3 light_pos = vec3(0,10,0);
uniform vec3 light_dir2 = vec3(1,-1,1);

// fénytulajdonságok: ambiens, diffúz, spekuláris
uniform vec3 La = vec3(0.4, 0.4, 0.4);
uniform vec3 Ld = vec3(0.4, 0.6, 0.6);
uniform vec3 Ls = vec3(1.0, 1.0, 1.0);

uniform vec3 La2 = vec3(1.0, 0, 0);
uniform vec3 Ld2 = vec3(0.7, 0.9, 0.7);
uniform vec3 Ls2 = vec3(1.0, 1.0, 1.0);

// anyagtulajdonságok: ambiens, diffúz, spekuláris
uniform vec3 Ka = vec3(0.2, 0.4, 0.6);
uniform vec3 Kd = vec3(0.2, 0.4, 0.6);
uniform vec3 Ks = vec3(1.0, 1.0, 1.0);

// kamera pozíció
uniform vec3 eye_pos;

uniform sampler2D texImage;
uniform sampler2D texImage2;
uniform float t;
uniform float T;

void main()
{	
	//
	// ambiens szín számítása
	//

	vec3 ambient = La * Ka;
	vec3 ambient2 = La2 * Ka;
	//
	// diffúz szín számítása
	//	
	/* segítség:
	    - normalizálás: http://www.opengl.org/sdk/docs/manglsl/xhtml/normalize.xml
	    - skaláris szorzat: http://www.opengl.org/sdk/docs/manglsl/xhtml/dot.xml
	    - clamp: http://www.opengl.org/sdk/docs/manglsl/xhtml/clamp.xml
	*/

	// fragmenthez tartozó normális
	vec3 normal = normalize(vs_out_norm);

	// fragmentből a fényforrás felé mutató vektor
	// irány fényforrás esetén:
	vec3 toLight = -normalize(light_dir);
	// pont fényforrás esetén:
	//vec3 toLight = normalize(light_pos - vs_out_pos);


	// diffúz együttható
	float di = clamp( dot( toLight, normal), 0.0f, 1.0f );

	vec3 diffuse = di * Ld * Kd;

	vec3 toLight2 = -normalize(light_dir2);
	float di2 = clamp( dot( toLight2, normal), 0.0f, 1.0f );
	vec3 diffuse2 = di2 * Ld2 * Kd;

	//
	// fényfoltképzõ szín
	//
	/* segítség:
		- reflect: http://www.opengl.org/sdk/docs/manglsl/xhtml/reflect.xml
		- power: http://www.opengl.org/sdk/docs/manglsl/xhtml/pow.xml
	*/

	// kamera felé mutató vektor
	vec3 e = normalize(eye_pos - vs_out_pos);

	// a felületről tökéletesen visszatükröződő fénysugár
	vec3 r = reflect(-toLight, normal);

	// spekuláris együttható
	float si = pow( clamp( dot(e, r), 0.0f, 1.0f ), 32);
	vec3 specular = si * Ls * Ks;

	vec3 r2 = reflect(-toLight2, normal);
	float si2 = pow( clamp( dot(e, r2), 0.0f, 1.0f ), 32);
	vec3 specular2 = si2 * Ls2 * Ks;

	//
	// a fragment végsõ színének meghatározása
	//

	//fs_out_col = vec4(ambient + diffuse + specular, 1);

	// felületi normális
	//fs_out_col = vec4(vs_out_norm, 1);
	//fs_out_col = vec4(ambient,1);

	float alpha = 0.5 * (sin(2 * 3.1415 * t / T)) + 0.5;

	// textúrával
	vec4 textureColor = texture(texImage, vs_out_tex);
	vec4 textureColor2 = texture(texImage2, vs_out_tex);
	// ha ezt használod az eredmény nem lesz jó, de legalább felettébb szórakoztató:
	//textureColor2 = texture(texImage2, alpha * vs_out_tex);
	//fs_out_col = vec4(ambient + diffuse + specular, 1) * textureColor;

	

	fs_out_col = vec4(ambient + diffuse + specular + ambient2 + diffuse2 + specular2, 1) * 
	((1 - alpha) * textureColor + alpha * textureColor2);

	//Egyenlítő kirajzolása
	if(abs(vs_out_tex.y-0.5) < 0.01)
	{
		fs_out_col = vec4(1,0,0,0);
	}

	//Hósapkák kirajzolása:
	if(vs_out_tex.y > 0.90 || vs_out_tex.y < 0.1)
	{
		fs_out_col = vec4(1,1,1,1);
	}


}

// Feladatok

// 1) Fényszámítás
// - ambiens
// - diffúz
// - spekuláris

// 2) Textúra
